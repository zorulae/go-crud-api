# go-crud-api

A simple movie library CRUD app built using Go.

## Prerequisites

[Go](https://go.dev/doc/install)

## Installation

```bash
# clone the repository
git clone https://gitlab.com/zorulae/go-crud-api.git

# navigate into the project directory
cd go-crud-api

# install dependencies
go get

# build the binary
go build -o app

# start the service
./app
```

The application should now be running on http://localhost:8080.
